# Fluxor

Fluxor est un projet django pour recevoir par email ses abonnements RSS.
3 interfaces de gestion :

* une API [drf](http://www.django-rest-framework.org/) WIP
* un site [vuejs](https://vuejs.org/) NEXT
* un bot email NEXT

Les tâches de mise à jour des flux sont lancées en arrière plan par des cron / timer systemd.

Le contenu complet des articles est extrait par [readability-lxml](https://pypi.org/project/readability-lxml/)