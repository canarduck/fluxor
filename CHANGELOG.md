# Journal des modifications

Ce fichier est généré automatiquement après chaque modification du dépôt. 
Les numéros indiqués correspondent aux [versions taggées](https://gitlab.com/canarduck/fluxor/tags).

## Version actuelle

### ★ Nouveautés

* Css foundation.
* Poc ajout feed depuis vuejs.
* Axios pour appels api.
* Paramètres CORS pour communication backend / frontend.
* Scss dans les .vue.
* Extraction des urls des flux sur une page.
* README.
* Commande purge de flux.
* Commande mise à jour de flux.
* Conversion de date feedparser / django.
* Extraction de texte avec readability.
* Première implémentation de l'api.
* Cache Articles.
* Modèles user, flux & abonnement.

### ⚔ Correctifs

* Uri unique pour les feeds.
* Champs api user.
* Api detection des flux.
* Dépendances image Docker.

### ✓ Autres

* Pkg: configuration django.
* Initial commit.

