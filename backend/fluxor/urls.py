"""
Routeur
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from rest_framework import routers

from core.views import UserViewSet
from feed.views import (FeedViewSet, SubscriptionViewSet, ArticleViewSet, DiscoverFeedsView)

router = routers.DefaultRouter()  # pylint: disable=C0103
router.register(r"feeds", FeedViewSet)
router.register(r"users", UserViewSet)
router.register(r"subscriptions", SubscriptionViewSet)
router.register(r"articles", ArticleViewSet)

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api/discover/(?P<url>.+)', DiscoverFeedsView.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
]
