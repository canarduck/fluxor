"""
Réglages django

Normaalement pas besoin de toucher ce fichier, les réglages se font dans le .env
cf. https://django-environ.readthedocs.io
"""

import os
import environ

root = environ.Path(__file__) - 3  # pylint: disable=C0103
env = environ.Env()  # pylint: disable=C0103
environ.Env.read_env()

# Personalisation Fluxor

FLUXOR_NAME = env('FLUXOR_NAME', default='fluxor')
FLUXOR_BASELINE = env('FLUXOR_BASELINE', default='Des flux RSS par mail')

# Chemins

SITE_ROOT = root()
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Applications & middleware

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
LOCAL_APPS = ['core', 'feed']
THIRD_PARTY_APPS = ['rest_framework', 'anymail', 'corsheaders']
INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS + THIRD_PARTY_APPS
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# Base de données

DATABASES = {'default': env.db()}

# Sécurité, Authentification et debug

AUTH_USER_MODEL = 'core.User'
SECRET_KEY = env('SECRET_KEY')
ALLOWED_HOSTS = tuple(env.list('ALLOWED_HOSTS', default=[]))
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
DEBUG = env('DEBUG')
TEMPLATE_DEBUG = DEBUG

# Internationalisation

LANGUAGE_CODE = 'fr-FR'
TIME_ZONE = 'CET'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Fichiers statiques, templates, routage & wsgi

www_root = root.path('www/')  # pylint: disable=C0103
STATIC_ROOT = www_root('static')
STATIC_URL = '/static/'
MEDIA_ROOT = www_root('media')
MEDIA_URL = 'media/'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
ROOT_URLCONF = 'fluxor.urls'
WSGI_APPLICATION = 'fluxor.wsgi.application'

# API

REST_FRAMEWORK = {
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
    'DEFAULT_VERSION': 'v1',
    #'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated', )
}
CORS_ORIGIN_ALLOW_ALL = True  # TODO: sécuriser la chose

# Emails

ANYMAIL = {
    'MAILGUN_API_KEY': env('MAILGUN_API_KEY'),
    'MAILGUN_SENDER_DOMAIN': env('MAILGUN_SENDER_DOMAIN'),
}

EMAIL_BACKEND = 'anymail.backends.mailgun.EmailBackend'
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL')
SERVER_EMAIL = DEFAULT_FROM_EMAIL
