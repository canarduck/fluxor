"""
Factory Boy factories
"""
from datetime import timedelta

import factory
from django.utils import timezone

from core.factories import UserFactory
from feed.models import Article, Feed, Subscription

# pylint: disable=R0903


class FeedFactory(factory.django.DjangoModelFactory):
    """
    Générateur de Feed
    """

    class Meta:
        model = Feed

    uri = factory.Faker('uri', locale='fr_FR')
    name = factory.Faker('company.name', locale='fr_FR')


class SubscriptionFactory(factory.django.DjangoModelFactory):
    """
    Générateur de Subscription
    """

    class Meta:
        model = Subscription

    feed = factory.SubFactory(FeedFactory)
    user = factory.SubFactory(UserFactory)


class ArticleFactory(factory.django.DjangoModelFactory):
    """
    Générateur d’Article
    """

    class Meta:
        model = Article

    feed = factory.SubFactory(FeedFactory)
    uri = factory.Faker('uri', locale='fr_FR')
    title = factory.Faker('company.bs', locale='fr_FR')
    body = factory.Faker('lorem.paragraph', locale='fr_FR')
    published_at = factory.fuzzy.FuzzyDateTime(timezone.now() - timedelta(days=30))
