"""
Sérialiseurs pour l'api
"""

from rest_framework import serializers

from feed.models import Article, Feed, Subscription


class FeedSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialisation d'un flux
    """

    class Meta:
        model = Feed
        fields = ('uri', 'name', 'checked_at', 'subscribers', 'articles')
        read_only_fields = ('articles', 'subscribers', 'checked_at')


class SubscriptionSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialisation d'un abonnement
    """

    class Meta:
        model = Subscription
        fields = ('feed', 'user', 'full_body')


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialisation d'un abonnement
    """

    class Meta:
        model = Article
        fields = ('feed', 'title', 'uri', 'authors', 'published_at', 'body', 'full_body',
                  'keywords', 'created_at')
