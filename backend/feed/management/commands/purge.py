"""
CLI de nettoyage de la base
"""
from django.core.management.base import BaseCommand
from feed.models import Article, Feed
from core.utils import CommandMixin


class Command(CommandMixin, BaseCommand):
    """Supprime les vieux articles et les flux sans abonnés"""

    help = __doc__

    def handle(self, *args, **options):
        """Purge"""

        count, _ = Article.objects.purge()
        self.display('{} article(s) supprimé(s)'.format(count), 'success')

        count, _ = Feed.objects.purge()
        self.display('{} flux supprimé(s)'.format(count), 'success')
