"""
CLI mise à jour de flux
"""
import feedparser
from django.core.management.base import BaseCommand
from django.utils import timezone
from feed.models import Article, Feed
from core.utils import struct_time_to_datetime, CommandMixin


class Command(CommandMixin, BaseCommand):
    """Récupère les articles des flux"""

    help = __doc__

    def handle(self, *args, **options):
        """Boucle sur les flux"""

        for feed in Feed.objects.all():
            self.display('Vérification de {}'.format(feed))
            doc = feedparser.parse(feed.uri, etag=feed.etag, modified=feed.modified_at)
            feed.name = doc['feed']['title']
            if not doc.entries:
                self.display('Pas de mise à jour depuis etag {etag} ou modified {modified}'.format(
                    etag=feed.etag, modified=feed.modified_at))
            for entry in doc.entries:
                article, created = Article.objects.update_or_create_from_entry(
                    feed=feed, entry=entry)
                if created:
                    self.display('Nouvel article: {}'.format(article), 'success')
                else:
                    self.display('Déjà en base : {}'.format(article), 'notice')
                # updated_at = datetime.fromtimestamp(mktime(entry.updated_parsed))
            try:
                feed.etag = doc.etag
            except AttributeError:
                pass
            try:
                feed.modified_at = struct_time_to_datetime(doc.modified_parsed)
            except AttributeError:
                pass
            feed.checked_at = timezone.now()
            feed.save()

        self.display('Les flux sont à jour', 'success')
