"""
Modèles pour les flux
"""
import logging
from datetime import timedelta
from requests import HTTPError

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Count
from django.utils import timezone
from django.utils.translation import gettext as _

from core import mailer
from core.utils import struct_time_to_datetime, get_url
from feed.utils import full_text

logger = logging.getLogger(__name__)  # pylint: disable=C0103


class FeedManager(models.Manager):
    """Manager dédié aux flux"""

    def purge(self) -> tuple:
        """
        Efface les Flux sans abonnés
        Pas possible de .delete() un qs annoté, d’où la requête intermédiaire pour supprimer depuis les pk
        """
        return (0, 0)


class Feed(models.Model):
    """
    Un flux
    """
    uri = models.URLField(_('URI'), unique=True)
    name = models.TextField(_('nom'), null=True)
    checked_at = models.DateTimeField(_('consulté le'), null=True)
    # @see https://pythonhosted.org/feedparser/http-etag.html
    etag = models.CharField(_('ETag'), max_length=254, null=True)
    modified_at = models.DateTimeField(_('modifié le'), null=True)
    subscribers = models.ManyToManyField(
        get_user_model(),
        through='Subscription',
        through_fields=('feed', 'user'),
    )
    objects = FeedManager()

    class Meta:
        """Classement"""
        ordering = ['uri']

    def __str__(self) -> str:
        name_uri = '{feed.name} <{feed.uri}>'.format(feed=self)
        return name_uri.strip()

    def subscribe(self, user: 'User') -> bool:
        """
        Abonnement de `email` à ce flux
        """
        _, created = Subscription.objects.get_or_create(feed=self, user=user)
        if created:
            logger.info('[DB] Abonnement de %s à %s', user, self)
            mailer.subscribe_success(self, user)
            return True
        logger.info('[DB] %s déjà abonne à %s', user, self)
        mailer.subscribe_duplicate(self, user)
        return False


class ArticleManager(models.Manager):
    """
    Manager dédiés aux articles
    """

    def update_or_create_from_entry(self, feed: Feed, entry: object) -> tuple:
        """
        Récupération ou création d'un article depuis une entrée RSS
        """
        created = False
        uri = entry.link
        try:
            published_at = entry.published_parsed
        except AttributeError:
            published_at = entry.updated_parsed
        published_at = struct_time_to_datetime(published_at)

        try:
            article = self.get(feed=feed, uri=uri)
            if article.published_at == published_at:
                # article non modifié depuis la dernière récup, on retourne
                return (article, False)
            # sinon on met à jour l'entrée
        except Article.DoesNotExist:
            article = Article(feed=feed, uri=uri)
            created = True
        article.published_at = published_at
        article.title = entry.title
        article.body = entry.summary
        # texte complet depuis la page
        try:
            response = get_url(uri)
            full_body = full_text(response.text)
            # si full_body est moins long que le summary on ignore
            if len(full_body) > len(article.body):
                article.full_body = full_body
        except HTTPError:
            pass
        authors = []
        try:
            authors.append(self.entry.author)
        except AttributeError:
            pass
        try:
            for contributor in self.entry.contributors:
                authors.append(contributor.name)
        except AttributeError:
            pass
        article.authors = ', '.join(authors)
        keywords = []
        try:
            for tag in self.entry.tags:
                keywords.append(tag.label if tag.label else tag.term)
        except AttributeError:
            pass
        article.keywords = ', '.join(keywords)
        article.save()
        return (article, created)

    def purge(self, days_to_keep: int = 50) -> int:
        """
        Efface les Articles créés depuis plus de `days_to_keep` jours
        """
        date = timezone.now() - timedelta(days=days_to_keep)
        return super().get_queryset().filter(created_at__lte=date).delete()


class Article(models.Model):
    """
    Cache d’un article de flux
    utiliser objects.update_or_create_from_entry() pour créer de nouveaux Article
    """
    feed = models.ForeignKey(Feed, related_name='articles', on_delete='CASCADE')
    uri = models.URLField(_('URI'))
    title = models.TextField(_('titre'), null=True)
    body = models.TextField(_('corps'), null=True)
    full_body = models.TextField(
        _('corps complet'),
        help_text=_(
            'Les flux peuvent être tronqués, on tente de récupérer l’article complet sur le site'),
        null=True)
    created_at = models.DateTimeField(
        _('enregistré le'),
        help_text=_('date de création de l’article dans la base'),
        auto_now_add=True)
    published_at = models.DateTimeField(
        _('publié le'),
        help_text=_('la date de publication ou la date de mise à jour'),
        null=True,
        blank=False)
    authors = models.TextField(_('auteurs'), null=True)
    keywords = models.TextField(_('mots clés'), null=True)
    objects = ArticleManager()

    class Meta:
        """Article unique par flux et tri par date"""
        ordering = ['published_at']
        unique_together = ('feed', 'uri')

    def __str__(self):
        return self.title


class Subscription(models.Model):
    """
    Un abonnement: un Feed + un User et quelques réglages (full_text, titre, etc.)
    """
    feed = models.ForeignKey(Feed, related_name='subscriptions', on_delete='PROTECT')
    user = models.ForeignKey(get_user_model(), related_name='subscriptions', on_delete='CASCADE')
    full_body = models.BooleanField(
        _('Récupérer le texte complet des articles'),
        help_text=_('Décocher si les mails sont vides ou si le contenu est incohérent'),
        default=True)
    last_sent_article_published_at = models.DateTimeField(
        _('Date dernier article envoyé'),
        help_text=_('Les articles d’une date antérieure ne seront pas transmis'),
        null=True,
        blank=True)

    # pylint: disable=R0903
    class Meta:
        """Abonnement unique"""
        unique_together = ('feed', 'user')

    def __str__(self):
        return str(self.feed)

    def send(self) -> int:
        """
        Envoi des articles dont la date de publication est supérieure à celle du dernier envoi
        Retourne le nombre d’Article envoyés
        """
        sent = 0
        for article in self.feed.articles.filter(
                published_at__gt=self.last_sent_article_published_at):
            self.last_sent_article_published_at = article.published_at
            sent += 1
        self.save()
        return sent
