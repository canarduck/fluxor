"""
Vues pour les flux
"""
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from feed.models import Article, Feed, Subscription
from feed.serializers import (ArticleSerializer, FeedSerializer, SubscriptionSerializer)
from feed.utils import discover_feeds
from core.utils import get_url


class FeedViewSet(viewsets.ModelViewSet):
    """
    Point d'entrée de l'API pour la gestion des flux
    """
    queryset = Feed.objects.all()
    serializer_class = FeedSerializer

    # def perform_create(self, serializer):
    #     """Abonne le user après création d'un flux"""
    #     feed = serializer.save()
    #     Subscription.objects.create(feed=feed, user=self.request.user)


class SubscriptionViewSet(viewsets.ModelViewSet):
    """
    Point d'entrée de l'API pour la gestion des abonnements
    """
    queryset = Subscription.objects.all()
    serializer_class = SubscriptionSerializer


class ArticleViewSet(viewsets.ModelViewSet):
    """
    Point d'entrée de l'API pour la gestion des articles
    """
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class DiscoverFeedsView(APIView):
    """
    Retourne la liste des flux trouvés sur l'url
    """

    # authentication_classes = (authentication.TokenAuthentication,)
    # permission_classes = (permissions.IsAdminUser,)

    def get(self, request, url, format=None):
        """Liste des flux"""
        response = get_url(url)
        feeds = discover_feeds(response.text, base_url=url)
        return Response({'url': url, 'feeds': feeds})


class SubscribeFeedView(APIView):
    """
    Abonnement à un flux
    """

    # authentication_classes = (authentication.TokenAuthentication,)
    # permission_classes = (permissions.IsAdminUser,)

    def get(self, request, url, format=None):
        """Abonnement à un flux"""
        feed, _ = Feed.objects.get_or_create(uri=url)
        feed.subscribe(self.request.user)
        feeds = discover_feeds(response.text, base_url=url)
        return Response({'url': url, 'feeds': feeds})