"""
Tests unitaires pour feed
"""

from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from feed.models import Article
from feed.factories import ArticleFactory, FeedFactory


class FeedTestCase(TestCase):
    """
    Tests du modèle Feed
    """

    def test_str(self):
        "Représentation textuelle d'un flux"
        uri = 'http://f.fr/f.xml'
        name = 'Tongan ninja'
        feed = FeedFactory(uri=uri, name=name)
        self.assertIn(uri, str(feed))
        self.assertIn(name, str(feed))


class ArticleTestCase(TestCase):
    """
    Tests du modèle Article
    """

    def test_purge(self):
        "Purge des articles de + de 31j"
        title = 'White diamond'
        ArticleFactory(created_at=timezone.now() - timedelta(days=33))
        ArticleFactory(created_at=timezone.now() - timedelta(days=34))
        ArticleFactory(title=title, created_at=timezone.now() - timedelta(days=30))
        ArticleFactory(created_at=timezone.now() - timedelta(days=31))
        ArticleFactory(created_at=timezone.now() - timedelta(days=32))
        self.assertEqual(Article.objects.count(), 5)
        purged, _ = Article.objects.purge()
        self.assertEqual(purged, 4)
        self.assertEqual(Article.objects.count(), 1)
        remaining = Article.objects.first()
        self.assertEqual(remaining.title, title)
