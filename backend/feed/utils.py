"""
Outils divers pour les flux
"""
from urllib.parse import urljoin

from lxml import html
from readability import Document

FEED_TYPES = [
    'application/rss+xml', 'text/xml', 'application/atom+xml', 'application/x.atom+xml',
    'application/x-atom+xml'
]


def full_text(page: str) -> str:
    """
    (tentative d')Extraction du contenu de l'article depuis le html pour obtenir le texte complet
    """
    try:
        document = Document(page)
    except ValueError:  # erreur dans readability
        return None
    return document.summary(html_partial=True)


def discover_feeds(page: str, base_url: str = None) -> list:
    """
    (tentative d')Extraction des adresses de flux sur une page :
    tous les <link> du type FEED_TYPES avec un href
    """

    def join(base_url, url):
        """urls absolues si relatives"""
        if '://' in url:
            return url
        return urljoin(base_url, url)

    # on utilise un parser qui ignore le plus possible les erreurs dans les pages
    tree = html.fromstring(page.encode(), base_url=base_url)
    links = tree.xpath('//link')
    return [
        join(base_url, link.get('href')) for link in links
        if link.get("type") in FEED_TYPES and link.get('href')
    ]
