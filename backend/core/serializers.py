"""
Sérialiseurs pour l'api
"""

from rest_framework import serializers

from core.models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialisation d'un utilisateur
    """

    password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ('id', 'name', 'email', 'password', 'subscriptions')
        extra_kwargs = {'password': {'write_only': True}}
