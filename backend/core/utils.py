"""
Outils divers pour fluxor
"""
from datetime import datetime
from time import struct_time
# from django.core.management.base import BaseCommand as DjangoBaseCommand
import requests
from faker import Faker
from django.utils import timezone


def struct_time_to_datetime(value: struct_time) -> datetime:
    """
    Conversion struct_time vers une datetime avec timezone
    utile pour convertir une date_parsed de feedparser vers une datetime django
    cf. https://pythonhosted.org/feedparser/date-parsing.html
    """
    return timezone.make_aware(timezone.datetime(*value[:-3]))


def get_url(url: str) -> object:
    """
    Retourne la response d'une request
    """
    fake = Faker('fr_FR')
    user_agent = fake.firefox()  # pylint: disable=E1101
    headers = {'User-Agent': user_agent}
    response = requests.get(url, headers=headers)
    response.raise_for_status()
    return response


class CommandMixin:
    """
    Quelques raccourcis pour BaseCommand histoire de faciliter les stdout.write
    """

    def display(self, msg: str, level: str = None):
        """
        Affiche msg stylisé au niveau level (notice, warning, error, success)
        Si level est None, on affiche sans style
        ex.
        * self.display('Bien joué', 'success')
        * self.display('Pour info')
        * self.display('Échec', 'error')
        """
        if level:
            method = getattr(self.style, level.upper())
            self.stdout.write(method(msg))
        else:
            self.stdout.write(msg)
