"""
Vues pour le cœur du projet
"""
from rest_framework import viewsets

from core.models import User
from core.serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    Point d'entrée de l'API pour la gestion des utilisateurs
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
