"""
Factory Boy factories
"""
import factory
from django.contrib.auth import get_user_model


class UserFactory(factory.django.DjangoModelFactory):
    """
    Générateur de User
    """

    class Meta:
        model = get_user_model()
        django_get_or_create = ('email', )

    email = factory.Faker('safe_email', locale='fr_FR')
    name = factory.Faker('name', locale='fr_FR')
    # password = factory.Faker('password')
