"""
Modèles pour le cœur de l'app
"""

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    """Manager pour User sans username"""

    use_in_migrations = True

    def _create_user(self, email: str, password: str, is_staff: bool, is_superuser: bool,
                     **extra_fields) -> 'User':
        """Création de l'utilisateur sans username"""
        if not email:
            raise ValueError(_('Email requis'))
        # email = self.normalize_email(email)
        email = email.lower()  # nique la RFC 5321 et particulièrement le point 2.4 paragraphe 2
        user = self.model(email=email, is_staff=is_staff, is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email: str, password: str = None, **extra_fields) -> 'User':
        """Nouveau User simple sans username"""
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email: str, password: str, **extra_fields) -> 'User':
        """Nouveau super User sans username"""
        return self._create_user(email, password, True, True, **extra_fields)


class User(AbstractUser):
    """Utilisateur sans username mais avec email"""

    username = None
    first_name = None
    last_name = None
    email = models.EmailField(_('courriel'), unique=True)
    name = models.CharField(
        _('nom'), max_length=100, help_text=_('Prénom Nom, pseudo, ...'), blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    EMAIL_FIELD = USERNAME_FIELD

    objects = UserManager()

    def __str__(self) -> str:
        return self.name or self.email

    def get_full_name(self) -> str:
        """pas de notion de prénom + nom, on utilise nom + <mail>"""
        full_name = '{user.name} <{user.email}>'.format(user=self)
        return full_name.strip()

    def get_short_name(self) -> str:
        return str(self)
